function filterBy(array, dataType) {
    return array.filter(function(item) {
        return typeof item !== dataType;
    });
}

let array = ['hello', 'world', 23, '23', null];
let filteredArray = filterBy(array, 'string');
console.log(filteredArray);